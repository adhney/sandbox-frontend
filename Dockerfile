# Use the official Node.js image as the base image
FROM node:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the frontend source code into the container
COPY . .

# Install dependencies
RUN yarn

# Build the Next.js application for production
# RUN yarn build

# Expose the port that Next.js uses (usually 3000, but change if you have modified it)
EXPOSE 3000

# Run the Next.js development server when the container starts
CMD ["yarn", "dev"]
